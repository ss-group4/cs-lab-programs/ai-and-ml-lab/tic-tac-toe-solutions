
import Board


# A variable to store turn of player. 1 is player 2 is AI


def play_game(diff: int = 1):
    board = Board.Board()
    is_player_turn = True
    while True:
        if not is_player_turn:
            board.turn = 2
            # Invoke AI function for move
            print("AI making move.")
            if diff == 1:
                board.medium_ai()
            if diff == 0:
                board.easy_ai()
            print(board.print_board())

            over, winner = board.win()
            if over:
                if board.winner is not None:
                    if board.winner == 2:
                        print(f"Game over  winner is AI")
                    else:
                        print("Game over winner is player")
                    break
                else:
                    print("Game is Draw")
            else:
                is_player_turn = True
        else:
            board.turn = 1
            print("Your turn.")
            board.print_board()
            print("Enter the x and y coordinates")
            move_x = int(input())
            move_y = int(input())

            if not board.place(move_x, move_y):
                is_player_turn = True
                print("Invalid Move")
                continue
            else:
                is_player_turn = False
            print(board.print_board())
            over, winner = board.win()
            if over:
                if board.winner is not None:
                    print(f"Game over  winner is {board.winner}")
                    break
                elif board.free == 0:
                    print("Game is Draw")
                    break


if __name__ == '__main__':
    x = int(input('Enter the mode: 0 for easy 1 for medium difficulty: '))
    play_game(x)

