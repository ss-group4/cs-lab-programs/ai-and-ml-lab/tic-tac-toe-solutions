import random


class Board:

    def __init__(self):
        self.over: bool = False
        self.board: list = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.winner = None
        self.turn = 1
        self.free = 9

    def make_copy(self):
        # Return a copy of board
        fake_board = list   ()
        for i in self.board:
            f = list()
            for j in i:
                f.append(j)
            fake_board.append(f)
        return fake_board

    # Function to print the board content
    def print_board(self):
        for i in range(len(self.board)):
            for j in range(len(self.board[0])):
                print(self.board[i][j], end=" ")
            print()
        print()

    def medium_ai(self):
        fake_board = self.make_copy()

        placed = False
        if self.turn == 2 and self.free != 0:
            for i in range(3):
                for j in range(3):
                    if fake_board[i][j] == 0:
                        fake_board[i][j] = 2  # place 2 on fake board
                        over, winner = self.win(fake_board)  # Check if AI can win
                        if over:
                            self.board[i][j] = 2  # Place on real board
                            return self.win()  # invoke game over function to declare AI as winner
                        else:
                            fake_board[i][j] = 0  # reset fake board[i][j] to 0

            for i in range(3):
                for j in range(3):
                    if fake_board[i][j] == 0:
                        # To check if player has winning chance
                        fake_board[i][j] = 1  # Place 1 in fake_board
                        over, winner = self.win(fake_board)  # Check if player has winning chance
                        if over:
                            self.board[i][j] = 2  # Place 2 in position where player has winning chance
                            self.free -= 1
                            x = self.win()  # Return position
                            return x
                        fake_board[i][j] = 0
            if not placed:
                no_random = self.non_random_move()
                if self.free != 0 and not no_random:
                    while True:
                        x, y = random.randint(0, 2), random.randint(0, 2)
                        # print(f"x is {x}and y is {y}")
                        if self.board[x][y] == 0:
                            self.board[x][y] = 2
                            self.free -= 1
                            break
                elif no_random:
                    return self.win()
                else:
                    print("Game is draw")
                    return None

    def easy_ai(self):
        if self.turn == 2:
            while True:
                x, y = random.randint(0, 2), random.randint(0, 2)
                # print(f"x is {x}and y is {y}")
                if self.board[x][y] == 0:
                    self.board[x][y] = 2
                    self.free -= 1
                    break

    def non_random_move(self):
        # Writing all the indexes of corner values to block player
        corner_indexes = [[0, 0], [0, 2], [1, 2], [2, 2], [2, 0]]
        for i in corner_indexes:
            if self.board[i[0]][i[1]] == 0:
                self.board[i[0]][i[1]] = 2
                self.free -= 1
                return True
        return False

    def place(self, x: int, y: int) -> bool:
        # if element == 0 return true
        # else false
        # print(f"DEBUG: x cord is {x}, y cord is {y} element at the cord is {self.board[x][y]}")

        if self.turn == 1 and self.free != 0:
            element = self.board[x][y]

            if element == 0:
                self.board[x][y] = 1
                # print("DEBUG PLACED 1")
                self.print_board()
                self.free -= 1
                return True

            else:
                return False

    def draw(self):
        if self.free == 0:
            self.over = True
            self.winner = None
            return True, None

    # Is game over function
    def is_game_over(self):
        over, winner = self.win()
        if over:
            self.over = True
            self.winner = winner
            return True, winner

    def win(self, board=None):
        # diagonal Check
        if board is None:
            board = self.board
        # Let us now check if game is draw or not first. for this we need to check if there is 0  in the board

        # Row check
        # print(board)
        # print("Row Check")
        for i in range(len(board)):
            if board[i][0] == board[i][1] and board[i][0] == board[i][2] and board[i][2] == board[i][1] \
                    and (board[i][0] == 1 or board[i][0] == 2) \
                    and (board[i][1] == 1 or board[i][1] == 2) and (board[i][2] == 1 or board[i][2] == 2):
                self.over = True
                self.winner = board[0][0]
                return self.over, self.winner

        # print("Column Check")
        self.print_board()
        # column check
        for i in range(len(board)):
            if board[0][i] == board[1][i] and board[2][i] == board[0][i] and board[1][i] == board[2][i] \
                    and (board[0][i] == 1 or board[0][i] == 2) \
                    and (board[1][i] == 1 or board[1][i] == 2) \
                    and (board[2][i] == 1 or board[2][i] == 2):
                # print(f" Game OVER!!!! After checking column {i}")
                self.over = True
                self.winner = board[0][i]
                return self.over, self.winner
        # print("Primary Diagonal Check")
        if board[0][0] == board[1][1] and board[0][0] == board[2][2] and (board[0][0] == 1 or board[0][0] == 2) and \
                (board[1][1] == 1 or board[1][1] == 2) and (board[2][2] == 1 or board[2][2] == 2):
            self.over = True
            self.winner = board[0][0]
            return self.over, self.winner
        # print("Secondary Diagonal")
        if board[2][0] == board[1][1] and board[0][2] == board[2][0] and board[1][1] == board[0][2] \
                and (board[2][0] == 1 or board[2][0] == 2) and \
                (board[1][1] == 1 or board[1][1] == 2) and (board[0][2] == 1 or board[0][2] == 2):
            self.over = True
            self.winner = board[2][0]
            return self.over, self.winner
        is_draw = True
        for i in board:
            for j in i:
                if j == 0:
                    return False, None
        if is_draw:
            self.over = True
            self.winner = None
            return True, None
        return False, None
